
//bai1 
function bai1() {
  var bai1Result = "";
  for (var i = 1; i < 101; i += 10) {
    for (var j = i; j < i + 10; j++) {
      bai1Result += `<td>${j}</td>`;
    }
    bai1Result = `<tr>${bai1Result}</tr>`;
  }
  document.getElementById("result_bai1").innerHTML = bai1Result;
}


//bai 2

var numArr = [];

function isPrime(number) {
  if (number < 2) {
    return 0;
  }

  for (var i = 2; i < number; i++) {
    if (number % i == 0) {
      return 0;
    }
  }
  return 1;
}

function themSo() {
  var themSoNguyen = document.getElementById("txt-nhap-so").value * 1;
  numArr.push(themSoNguyen);
  document.getElementById("txt-mang-so").innerText = numArr;
  document.getElementById("txt-nhap-so").value = "";

}

function timSoNguyen() {
  document.querySelector("#result_bai2").innerText = ``;

  for (var i = 0; i < numArr.length; i++) {
    if (isPrime(numArr[i]) == 1) {
      document.querySelector("#result_bai2").innerHTML += `${numArr[i]}  `;
    }
  }
}



//bai 3:

function nhapSoN() {
  var soN = document.getElementById("txt-so-n").value * 1;
  var ketQua = 0;
  for (var i = 2; i <= soN; i++) {
    ketQua = ketQua + i;
  }
  ketQua = ketQua + 2 * soN;
  document.getElementById("result_bai3").innerHTML = ketQua;
}


//bai 4

function uocSoN() {
  var uocSoArr = [];
  var uocSo = document.getElementById("txt-so-n4").value * 1;
  for (var i = uocSo; i >= 1; i--) {
    if (uocSo % 1 == 0) {
      uocSoArr.push(i);
    }
  }
  document.getElementById("result_bai4").innerHTML = uocSoArr
}


// bai 5 : 

function bai5(str) {
  // Step 1. Use the split() method to return a new array
  var splitString = str.split(""); // var splitString = "hello".split("");
  // ["h", "e", "l", "l", "o"]

  // Step 2. Use the reverse() method to reverse the new created array
  var reverseArray = splitString.reverse(); // var reverseArray = ["h", "e", "l", "l", "o"].reverse();
  // ["o", "l", "l", "e", "h"]

  // Step 3. Use the join() method to join all elements of the array into a string
  var joinArray = reverseArray.join(""); // var joinArray = ["o", "l", "l", "e", "h"].join("");
  // "olleh"
  
  //Step 4. Return the reversed string
  return joinArray; // "olleh"
}


function soDao() {
  var soDao = document.getElementById("txt-so-n5").value;

  let res=bai5(soDao)
  console.log(res);
  document.getElementById("result_bai5").innerHTML = res;
}


//bai6 
function soNguyenMax() {
  var soX = 0;
  /*var i =0;
  while (soX <=100) {
    soX += i;
  }
  return - 2*/
  for (var i = 1; i < 100; i++) {
    if (soX <= 100 - i) {
      soX = soX + i;
    } else {
      break;
    }
  }
  i--;
  document.getElementById("result_bai6").innerHTML = i;
}


//bai 7

function bangCuuChuong() {
  var soN7 = document.getElementById("txt-so-n7").value * 1;
  var cuuChuong = "";
  for (var i = 0; i <= 10; i++) {
    cuuChuong += `${soN7} x ${i} = ${soN7 * i} </br>`;
  }
  document.getElementById("result_bai7").innerHTML = cuuChuong;

}

//bai8

var cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
function chiaBai() {
  var bonNguoiChoi = [[], [], [], []];
  var ketQuaChia = "";
  for (i = 0; i < 4; i++) {
    for (j = 0; j < cards.length; j++) {
      if (j % 4 == i) {
        bonNguoiChoi[i].push(cards[j]);
      }
    }

    ketQuaChia += `<h5 class="mt-3">Player ${i + 1}: ${bonNguoiChoi[i]} </br></h5> `;
  }
  document.getElementById("result_bai8").innerHTML = ketQuaChia;
}

//bai 9: ket qua sai
function choGa() {
  var m = document.querySelector("#txt-con-choga").value * 1;
  var n = document.querySelector("#txt-chan-choga").value * 1;
  bai9ChoGa = "";

  for (var i = 1; i <= m; i++) {
    if (i * 2 + (m - i) * 4 == n) {
      ketQuaEl.innerHTML = `${i} con gà, ${m - i} con chó`;
    }
  }
  document.getElementById("result_bai9").innerHTML = `<p>So ga la: ${i}, So cho la:  ${m - i}</p>`
}

//bai10
function gocLech() {

  var hour = document.getElementById("txt-so-gio").value * 1;
  var minu = document.getElementById("txt-so-phut").value * 1;
  var gocGio = (hour / 12) * 360 + ((minu / 60) * 360) / 12;
  var gocPhut = (minu / 60) * 360;

  document.getElementById("result_bai10").innerText = `Goc nho la ${Math.abs(gocGio - gocPhut)} do, Goc 2 la ${360 - Math.abs(gocGio - gocPhut)} do`;

}




